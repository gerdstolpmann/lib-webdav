This library implements the client side of the WebDAV protocol
(RFC 4918). The file locking part of WebDAV is omitted, though.

The library uses Ocamlnet's [netclient] library for client-side
HTTP, and extends it by providing the required access methods
for WebDAV. Additionally, there is also an implementation for
Ocamlnet's [Netfs.stream_fs] abstraction modelling a simple
file system.

This work is sponsored and property of Mylife.com, Los Angeles, CA.
It can be used by everyone according to the license agreement. The
author is Gerd Stolpmann, gerd\@gerd-stolpmann.de.

{!modules:
  Webdav_client_methods
  Webdav_client
  Webdav_client_ha
  Webdav_netfs
  Webdav_http
  Webdav_xml
}
