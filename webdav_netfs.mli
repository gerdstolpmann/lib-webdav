(* $Id$ *)

(** {1 WebDAV support for the [Netfs] API} *)

(**
  Allows access to WebDAV in the style of [Netfs.stream_fs].
   [Netfs] is part of the Ocamlnet library [netstring], and provides
   a halfway-standard API to access various types of filesystems.
 *)

(** {2 Extended [stream_fs] type} *)

(** We support not only the basic {!Netfs.stream_fs} API, but also the
    extensions of the {!Http_fs.http_stream_fs} plus an additional
    method [webdav_client] that just returns the internal client
    object.

    Links:
    - {{:http://projects.camlcity.org/projects/dl/ocamlnet-3.2/doc/html-main/Netfs.html} Netfs}
    - {{:http://projects.camlcity.org/projects/dl/ocamlnet-3.2/doc/html-main/Http_fs.html} Http_fs}
 *)


class type webdav_stream_fs =
object
  inherit Nethttp_fs.http_stream_fs
  method last_response_status : Nethttp.http_status * int * string
    (** This is the status method inherited from [Http_fs]. Note that it
        cannot report Webdav-specific codes like [`Multi_status]. These
        codes are mapped to base codes (e.g. [`Ok]).
     *)
  method last_webdav_response_status : Webdav_http.webdav_status * int * string
    (** This is a specialized version of [last_response_status] also
        supporting Webdav-specific status codes.
     *)
  method webdav_client : Webdav_client.webdav_client_t
    (** The underlying WebDAV client *)
end

(** {2 The class} *)

class webdav_netfs : ?tmp_directory:string -> ?tmp_prefix:string ->
                     ?ip:Unix.inet_addr ->
                     string -> webdav_stream_fs
val webdav_netfs : ?tmp_directory:string -> ?tmp_prefix:string ->
                   ?ip:Unix.inet_addr ->
                   string -> webdav_stream_fs
  (** [webdav_netfs baseurl]:
      The path [/] in the access methods corresponds to the passed [base_url]
      argument (like in {!Webdav_client.webdav_client_t}).

      Paths are encoded in UTF-8.

      There are no symlinks in WebDAV, so the symlink part of [stream_fs]
      remains unimplemented (methods [symlink] and [readlink], and the
      symlink-specific file tests).

      The path components [.] and [..] are resolved on the client side
      before a URL is sent to the server (i.e. "nominal" [..] resolution).

      Remarks to the access methods:
      - [read]: Works exactly as in {!Nethttp_fs.http_fs}. The [`Streaming]
        and [`Skip] flags are supported.
      - [write]: Works exactly as in {!Nethttp_fs.http_fs}. The [`Streaming],
        [`Truncate], [`Create] and [`Exclusive] flags are supported.
        It is not possible, though, to overwrite existing files without
        truncation.
      - [size]: supported
      - [test] and [test_flags]: Non-directories are reported as regular
        files (there is no other way to check for this). There is no
        good support for [`R], [`W], and [`X] - for simplicity, [`R] and
        [`W] are always true, [`X] is an alias for [`D].
      - [mkdir]: supported
      - [remove]: supported. WebDAV only knows about recursive deletes.
        When the [`Recursive] flag is not passed, it is first checked
        whether the resource is a directory, and if so, the deletion is
        rejected. This is a quite cheap emulation, though.
      - [rmdir]: is not supported. Use a recursive [remove] instead.
      - [readdir]: is supported
      - [rename]: is supported. Note that the destination path is
        always removed if it exists, even if it points to a non-empty
        directory
      - [copy]: is supported (of course, only for non-recursive copies)

      Options:
      - [tmp_directory]: Where temporary files are created
      - [tmp_prefix]: Prefix for temporary files
      - [ip]: If passed, the server will be contacted at this IP, overriding
        any hostname lookups.

      Use together with {!Webdav_client_ha} as in:

      {[
        let real_url, ip = Webdav_client_ha.select_endpoint [urls] timeout in
        let netfs = Webdav_netfs.webdav_netfs ~ip real_url in ...
      ]}

   *)


class webdav_netfs_layer : ?ip:Unix.inet_addr ->
                           Nethttp_fs.http_fs -> webdav_stream_fs
val webdav_netfs_layer : ?ip:Unix.inet_addr ->
                         Nethttp_fs.http_fs -> webdav_stream_fs
  (** [webdav_netfs_layer http_fs]: Same as above, but the WebDAV protocol
      is added on top of an already existing [http_fs].

      It is required that [http_fs] uses UTF-8 as path encoding.
   *)


val norm_path : string -> string
val translate_status : Webdav_http.webdav_status -> string -> exn
