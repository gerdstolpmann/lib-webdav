# WebDAV

This is a fork of the webdav library, originally released by
Mylife.com, Inc., on their website http://oss.wink.com.

 - [Project page](http://projects.camlcity.org/projects/webdav.html)

 - [Changelog (master)](ChangeLog)

 - [Install](INSTALL)

 - [License](LICENSE)

