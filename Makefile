.PHONY: build all doc install release clean postconf

version = 1.1.5
fullname = webdav-$(version)

trunk = https://godirepo.camlcity.org/svn/lib-webdav/trunk
tag = https://godirepo.camlcity.org/svn/lib-webdav/tags/webdav-$(version)


build:
	omake

all:
	omake

doc:
	cd doc; $(MAKE) doc

install:
	ocamlfind install webdav \
		META *.mli *.cmi webdav.cma \
		-optional webdav.cmxa webdav.a \
		-patch-version "$(version)"

clean:
	omake clean

postconf:
	echo 'pkg_version="$(version)"' >>setup.data


FILES = \
  *.ml *.mli \
  META \
  Makefile \
  OMakefile \
  OMakeroot \
  configure \
  INSTALL \
  LICENSE \
  ChangeLog \
  README \
  _oasis

release: _oasis
	if [ ! -d doc/html-main ]; then echo "No docs!"; exit 1; fi
	mkdir -p release
	rm -rf release/$(fullname)
	mkdir release/$(fullname)
	mkdir release/$(fullname)/doc
	mkdir release/$(fullname)/doc/html-main
	cp $(FILES) release/$(fullname)
	cp doc/html-main/*.html release/$(fullname)/doc/html-main
	cp doc/html-main/*.css release/$(fullname)/doc/html-main
	cd release && tar czf $(fullname).tar.gz $(fullname)
	@echo "*** Run 'make tag' to tag the release"

.PHONY: tag
tag:
	master="$$(git branch | grep '* master')"; \
	if [ -z "$$master" ]; then \
	    echo "Error: not on master branch"; \
	    exit 1; \
	fi
	@status="$$(git status -uno -s)"; \
	if [ -n "$$status" ]; then echo "Error: git status not clean"; exit 1; fi
	git tag -a -m "webdav-$(version)" webdav-$(version)
	git push --tags origin master
	@echo "New tag: webdav-$(version)"


.PHONY: _oasis
_oasis: _oasis.in
	sed -e 's/@VERSION@/$(version)/' _oasis.in >_oasis
	oasis setup

