(* Examples taken from the RFC text *)

let r1 = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<D:multistatus xmlns:D=\"DAV:\">
  <D:response xmlns:R=\"http://ns.example.com/boxschema/\">
    <D:href>http://www.example.com/file</D:href>
    <D:propstat>
      <D:prop>
	<R:bigbox>
	  <R:BoxType>Box type A</R:BoxType>
	</R:bigbox>
	<R:author>
	  <R:Name>J.J. Johnson</R:Name>
	</R:author>
      </D:prop>
      <D:status>HTTP/1.1 200 OK</D:status>
    </D:propstat>
    <D:propstat>
      <D:prop><R:DingALing/><R:Random/></D:prop>
      <D:status>HTTP/1.1 403 Forbidden</D:status>
      <D:responsedescription> The user does not have access to the
 DingALing property.
      </D:responsedescription>
    </D:propstat>
  </D:response>
  <D:responsedescription> There has been an access violation error.
  </D:responsedescription>
</D:multistatus>"


let r2 = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<multistatus xmlns=\"DAV:\">
  <response>
    <href>http://www.example.com/container/</href>
    <propstat>
      <prop xmlns:R=\"http://ns.example.com/boxschema/\">
	<R:bigbox/>
	<R:author/>
	<creationdate/>
	<displayname/>
	<resourcetype/>
	<supportedlock/>
      </prop>
      <status>HTTP/1.1 200 OK</status>
   </propstat>
  </response>
  <response>
    <href>http://www.example.com/container/front.html</href>
    <propstat>
      <prop xmlns:R=\"http://ns.example.com/boxschema/\">
	<R:bigbox/>
	<creationdate/>
	<displayname/>
	<getcontentlength/>
	<getcontenttype/>
	<getetag/>
	<getlastmodified/>
	<resourcetype/>
	<supportedlock/>
      </prop>
      <status>HTTP/1.1 200 OK</status>
    </propstat>
  </response>
</multistatus>"

let r3 = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<D:multistatus xmlns:D=\"DAV:\">
  <D:response>
    <D:href>/container/</D:href>
    <D:propstat>
      <D:prop xmlns:R=\"http://ns.example.com/boxschema/\">
	<R:bigbox><R:BoxType>Box type A</R:BoxType></R:bigbox>
	<R:author><R:Name>Hadrian</R:Name></R:author>
	<D:creationdate>1997-12-01T17:42:21-08:00</D:creationdate>
	<D:displayname>Example collection</D:displayname>
	<D:resourcetype><D:collection/></D:resourcetype>
	<D:supportedlock>
	  <D:lockentry>
	    <D:lockscope><D:exclusive/></D:lockscope>
	    <D:locktype><D:write/></D:locktype>
	  </D:lockentry>
	  <D:lockentry>
	    <D:lockscope><D:shared/></D:lockscope>
	    <D:locktype><D:write/></D:locktype>
	  </D:lockentry>
	</D:supportedlock>
      </D:prop>
      <D:status>HTTP/1.1 200 OK</D:status>
    </D:propstat>
  </D:response>
  <D:response>
    <D:href>/container/front.html</D:href>
    <D:propstat>
      <D:prop xmlns:R=\"http://ns.example.com/boxschema/\">
	<R:bigbox><R:BoxType>Box type B</R:BoxType>
	</R:bigbox>
	<D:creationdate>1997-12-01T18:27:21-08:00</D:creationdate>
	<D:displayname>Example HTML resource</D:displayname>
	<D:getcontentlength>4525</D:getcontentlength>
	<D:getcontenttype>text/html</D:getcontenttype>
	<D:getetag>\"zzyzx\"</D:getetag>
	<D:getlastmodified
	  >Mon, 12 Jan 1998 09:25:56 GMT</D:getlastmodified>
	<D:resourcetype/>
	<D:supportedlock>
	  <D:lockentry>
	    <D:lockscope><D:exclusive/></D:lockscope>
	    <D:locktype><D:write/></D:locktype>
	  </D:lockentry>
	  <D:lockentry>
	    <D:lockscope><D:shared/></D:lockscope>
	    <D:locktype><D:write/></D:locktype>
	  </D:lockentry>
	</D:supportedlock>
      </D:prop>
      <D:status>HTTP/1.1 200 OK</D:status>
    </D:propstat>
  </D:response>
</D:multistatus>"

let r4 = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<D:multistatus xmlns:D=\"DAV:\"
	xmlns:Z=\"http://ns.example.com/standards/z39.50/\">
  <D:response>
    <D:href>http://www.example.com/bar.html</D:href>
    <D:propstat>
      <D:prop><Z:Authors/></D:prop>
      <D:status>HTTP/1.1 424 Failed Dependency</D:status>
    </D:propstat>
    <D:propstat>
      <D:prop><Z:Copyright-Owner/></D:prop>
      <D:status>HTTP/1.1 409 Conflict</D:status>
    </D:propstat>
    <D:responsedescription> Copyright Owner cannot be deleted or
      altered.</D:responsedescription>
  </D:response>
</D:multistatus>"

let r5 = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<d:multistatus xmlns:d=\"DAV:\">
  <d:response>
    <d:href>http://www.example.com/container/resource3</d:href>
    <d:status>HTTP/1.1 423 Locked</d:status>
    <d:error><d:lock-token-submitted/></d:error>
  </d:response>
</d:multistatus>"

let r6 = "<d:multistatus xmlns:d=\"DAV:\">
  <d:response>
    <d:href>http://www.example.com/othercontainer/R2/</d:href>
    <d:status>HTTP/1.1 423 Locked</d:status>
    <d:error><d:lock-token-submitted/></d:error>
  </d:response>
</d:multistatus>"

