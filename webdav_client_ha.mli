(* $Id$ *)

(** High-availability add-on for the WebDAV client *)

open Webdav_client

exception Service_unavailable of string

val select_endpoint : string list -> float -> string * Unix.inet_addr
  (** [let base_url, base_ip = select_endpoint base_urls tmo]: 
      The input list is a list of base URLs (as passed to [webdav_client]).
      The function pings these URLs, and selects the most quickly responding
      base URL. This URL is returned, together with the IP address that
      was used for the ping. In [tmo] the timeout for the ping is specified.

      This ping consists of sending an "OPTIONS" request to the server.

      Example:
      {[
        select_endpoint [ "http://h1/path"; "http://h2/path" ] 5.0
      ]}

      might return [ ("http://h2/path", "192.168.76.20") ] if this URL
      is chosen, together with the resolution of [h2] that was used during
      the ping.

      If an error occurs, [Service_unavailable] is raised, with the 
      argument describing the error.
   *)

val webdav_client : ?pipeline : Nethttp_client.pipeline ->
                    string * Unix.inet_addr -> webdav_client_t
  (** [let wc = webdav_client (base_url,base_ip)]:
      Configures the WebDAV client [wc] so that the result of a prior
      [select_endpoint] is incorporated. Effectively, all WebDAV accesses
      use the passed IP address.
   *)
