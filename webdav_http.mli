(* $Id$ *)

(** Extension of [Nethttp] for WebDAV support *)

type webdav_status =
    [ Nethttp.http_status
    | `Multi_status
    | `Unprocessable_entity
    | `Locked
    | `Failed_dependency
    | `Insufficient_storage
    ]

val int_of_webdav_status : webdav_status -> int
  (** Returns the integer code for a status value *)

val string_of_webdav_status : webdav_status -> string
  (** The explanatory text *)

val webdav_status_of_int : int -> webdav_status
  (** Returns the status value for an integer code, or raises [Not_found] *)

val webdav_proto : string
  (** "HTTP/1.1" - what is put into generated inner status lines *)

type depth = [ `Zero | `One | `Infinity ]
  (** Whether the operation affects only the request URL, the children
      of the request URL, or the whole tree there
   *)

type dav = [ `Class1 | `Class2 | `Class3 | `Token of string | `URL of string ]
  (** DAV compliance *)


(** {2 Representation of [If] header} *)

(** The following type definition allows readable [If] expressions.
    For example,

    {[
       (<urn:uuid:181d4fae-7d8c-11d0-a765-00a0c91e6bf2>
         ["I am an ETag"])
         (["I am another ETag"])
    ]}

    would be represented as

    {[
      `Self_target
         (`Or
            [`And
               [`State_token "urn:uuid:181d4fae-7d8c-11d0-a765-00a0c91e6bf2";
                `Etag(`Strong "I am an ETag")
               ];
             `And
               [`Etag(`Strong "I am another ETag")]
            ]
         )
    ]}
 *)


type if_condition =
  [ `Self_target of if_self_or_condition
  | `URL_target of if_url_or_condition 
  ]
  (** A [`Self_target] is an [If] expression about the request URI of the
      request including the [If] header. A [`URL_target] is an [If] 
      expression about arbitrary URLs.
   *)

and if_self_or_condition =
    [ `Or of if_and_condition list ]

and if_url_or_condition =
    [ `Or of (string * if_and_condition) list ]

and if_and_condition =
    [ `And of if_atom list ]

and if_atom =
    [ `Not of if_atom
    | `State_token of string
    | `Etag of Nethttp.etag
    ]
  (** An atom of an [If] expression can be negated, or it can be
      a state token (lock token), or it can be an etag.

      Note that state tokens are URIs, and reserved characters must
      be escaped to avoid confusion of the recipient of the message.
      Reserved characters are here especially double quotes, and
      "<" and ">" and spaces.
   *)


(** This module contains extensions of {!Nethttp.Header} for parsing
    and setting header fields of WebDAV requests and responses.

    For non-existing fields [Not_found] is raised. In case of unparseable
    fields {!Nethttp.Bad_header_field} is raised. For wrong input argument
    for [set_*] functions [Invalid_argument] is raised.
 *)
module Header : sig

  val get_dav : #Nethttp.http_header_ro -> dav list
  val set_dav : #Nethttp.http_header -> dav list -> unit

  val get_depth : #Nethttp.http_header_ro -> depth
  val set_depth : #Nethttp.http_header -> depth -> unit

  val get_destination : #Nethttp.http_header_ro -> string
  val set_destination : #Nethttp.http_header -> string -> unit

  val get_overwrite : #Nethttp.http_header_ro -> bool
  val set_overwrite : #Nethttp.http_header -> bool -> unit

  val get_if : #Nethttp.http_header_ro -> if_condition
  val set_if : #Nethttp.http_header -> if_condition -> unit

  (* Omitted for now:
     - Lock-token
     - Timeout
   *)

end
